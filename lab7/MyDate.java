public class MyDate {
    private int day, month, year;
    private static int[] maxDays = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month - 1;
        this.year = year;
    }

    public String toString() {
        return "" + year + "-" + (month < 9 ? "0" : "") + (month + 1) + "-" + (day < 10 ? "0" : "") + day;
    }

        public void incrementDay() {
            if(isLeapYear()) {
                maxDays[1] = 29;
            } else {
                maxDays[1] = 28;
            }
            day++;
            if(day > maxDays[month]) {
                day = 1;
                incrementMonth();
            }
        }

    public void incrementYear(int value) {
        for(int i=0; i<value; i++) {
            incrementYear();
        }
    }


    public void decrementDay() {
        if(isLeapYear()) {
            maxDays[1] = 29;
        } else {
            maxDays[1] = 28;
        }
        day--;
        if(day == 0) {
            decrementMonth();
            day = maxDays[month];

        }
    }


    public void decrementYear() {
        year--;
        if(!isLeapYear() && month == 1 && day == 29) {
            day = 28;
        } else if(isLeapYear() && month == 1 && day == 28) {
            day = 29;
        }
    }


    public void decrementMonth() {
        if(isLeapYear()) {
            maxDays[1] = 29;
        } else {
            maxDays[1] = 28;
        }

        if(month == 0) {
            decrementYear();
            month = 12;
        }
        month--;
        if(day > maxDays[month]) {
            day = maxDays[month];
        }
    }



    public void incrementDay(int value) {
        for (int i=0; i<value; i++) {
            incrementDay();
        }
    }


    public void decrementMonth(int value) {
        for(int i=0; i<value; i++) {
            decrementMonth();
        }
    }


    public void decrementDay(int value) {
        for(int i=0; i<value; i++) {
            decrementDay();
        }
    }

    public void incrementMonth(int value) {
        for(int i=0; i<value; i++) {
            incrementMonth();
        }

        if(isLeapYear() && day > maxDays[month]) {
            day = maxDays[month];
        }
    }

    public void decrementYear(int value) {
        for(int i=0; i<value; i++) {
            decrementYear();
        }
    }

    public void incrementMonth() {
        if(isLeapYear()) {
            maxDays[1] = 29;
        } else {
            maxDays[1] = 28;
        }
        month++;
        if(month > 11) {
            incrementYear();
            month=0;
        }
    }


    public void incrementYear() {
        year++;

        if(!isLeapYear() && month == 1 && day == 29) {
            day = 28;
        }
    }
    private boolean isLeapYear() {
        return year % 4 == 0;
    }

    public boolean isBefore(MyDate date) {
        if(this.year < date.getYear() || this.month < date.getMonth() || this.day < date.getDay()) {
            return true;
        }
        return false;
    }

    public boolean isAfter(MyDate date) {
        if(this.year >date.getYear() || this.month > date.getMonth() || this.day > date.getDay()) {
            return true;
        }
        return false;
    }


    public int dayDifference(MyDate anotherDate) {
        MyDate fakeThis = new MyDate(day, month, year);
        MyDate fakeAnother =  new MyDate(anotherDate.getDay(), anotherDate.getMonth(), anotherDate.getYear());

        MyDate biggerDate = isBefore(anotherDate) ? anotherDate : this;
        MyDate smallerDate = isBefore(anotherDate) ? this : anotherDate;


        int cnt = 0;
        while(!(this.year == anotherDate.getYear() &&
                this.month == anotherDate.getMonth() &&
                this.day == anotherDate.getDay())) {
//            System.out.println(this.toString() + " ** " + anotherDate.toString());
            smallerDate.incrementDay();

            cnt++;
        }

        this.day = fakeThis.getDay();
        this.month = fakeThis.getMonth() + 1;
        this.year = fakeThis.getYear();

        anotherDate.setDay(fakeAnother.getDay());
        anotherDate.setMonth(fakeAnother.getMonth() + 1);
        anotherDate.setYear(fakeAnother.getYear());


        return cnt;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }
}


