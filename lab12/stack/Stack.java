package stack;

import javafx.beans.property.ObjectProperty;

public interface Stack {
    public void push(Object item);
    public Object pop();
    public boolean empty();
}
