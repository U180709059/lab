package stack;

public class StackImpl implements Stack {
   protected StackItem top=null;
    @Override
    public void push(Object item) {
        StackItem previusTop=top;
        top=new StackItem(item);
        top.setNext(previusTop);
    }

    @Override
    public Object pop() {
        StackItem previusTop=top.getNext();
        StackItem item=top;
        top=previusTop;
        return item.getItem();
    }

    @Override
    public boolean empty() {
        return top==null;
    }
}
