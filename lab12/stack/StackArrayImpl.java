package stack;

import java.util.ArrayList;

public class StackArrayImpl implements Stack {

   protected ArrayList<StackItem> stack=new ArrayList<>();
    @Override
    public void push(Object item) {
        stack.add(new StackItem(item));

    }

    @Override
    public Object pop() {
        return stack.remove(stack.size()-1);
    }

    @Override
    public boolean empty() {
        return stack.isEmpty();
    }
}
