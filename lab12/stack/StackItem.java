package stack;

public class StackItem {
    protected Object item;
    protected StackItem next;

    public StackItem(Object item) {
        this.item = item;
    }

    public Object getItem() {
        return item;
    }

    public StackItem getNext() {
        return next;
    }

    public void setNext(StackItem next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return ""+item;
    }
}
