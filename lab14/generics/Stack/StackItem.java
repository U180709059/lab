package generics.Stack;

public class StackItem <T>{
    protected T item;
    protected StackItem<T> next;

    public StackItem(T item) {
        this.item = item;
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item){this.item=item;}
    public StackItem<T> getNext() {
        return next;
    }

    public void setNext(StackItem<T> next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return ""+item;
    }
}
