package generics.Stack;

import java.util.ArrayList;
import java.util.List;

public class StackImpl<T> implements Stack<T> {
   protected StackItem<T> top=null;
    @Override
    public void push(T item) {
        StackItem<T> previusTop=top;
        top=new StackItem(item);
        top.setNext(previusTop);
    }

    @Override
    public T pop() {
        StackItem<T> previusTop=top.getNext();
        StackItem<T> item=top;
        top=previusTop;
        return item.getItem();
    }

    @Override
    public boolean empty() {
        return top==null;
    }

    @Override
    public List<T> toList() {
        List<T> answer=new ArrayList<>();
        StackItem<T> item=top;
        while (item!=null){
            answer.add(item.getItem());
            StackItem<T>nextItem=item.getNext();
        }
        return null;
    }

    @Override
    public void addAll(Stack<? extends T> aStack) {
        for (int i=aStack.toList().size()-1; i>=0;i--){
            this.push(aStack.toList().get(i));
        }
    }
}
