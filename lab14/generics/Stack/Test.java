package generics.Stack;

public class Test {
    public static void main(String[] args) {
        Stack<Object> stack = new StackArrayImpl<>();
        Stack<String> stack2 = new StackArrayImpl<>();
        stack.push("A");
        stack.push("B");
        stack.push("C");
        stack.push("D");

        stack2.push("E");
        stack2.push("F");
        stack2.push("G");
        stack2.push("H");


        stack.addAll(stack2);
        System.out.println(stack.toList());

        //String str = stack.pop();
        //stack.pop();
        //System.out.println(stack.pop());

    }


}
