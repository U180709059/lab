package generics.Stack;

import java.util.ArrayList;
import java.util.List;

public class StackArrayImpl<T> implements Stack<T> {

   protected ArrayList<StackItem<T>> stack=new ArrayList<>();
    @Override
    public void push(T item) {
        stack.add(0,new StackItem<T>(item));

    }

    @Override
    public T pop() {
        StackItem<T>item=stack.remove(0);
        return item.getItem();
    }

    @Override
    public boolean empty() {
        return stack.isEmpty();
    }

    @Override
    public List<T> toList() {
        List<T> answer = new ArrayList<>();
        for (int i=0; i<stack.size();i++){
            answer.add(stack.get(i).getItem());
        }
        return answer;
    }

    @Override
    public void addAll(Stack<? extends T> aStack) {
        for (int i=aStack.toList().size()-1; i>=0;i--){
            this.push(aStack.toList().get(i));
        }
    }
}
