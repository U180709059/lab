package gaye.shape;

public class Circle {
    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }
    public double area(){
        return Math.PI*this.radius*this.radius;
    }
}
