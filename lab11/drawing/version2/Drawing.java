package drawing.version2;

import java.util.ArrayList;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;

public class Drawing {
	private ArrayList<Object> objects = new ArrayList<>();


	public double calculateTotalArea() {
		double totalArea = 0;

		for (Object object : objects) {
			if (object instanceof Circle) {
				totalArea += ((Circle) object).area();
			} else if (object instanceof Rectangle) {
				totalArea += ((Rectangle) object).area();
			} else {
				totalArea += ((Square) object).area();
			}
		}
		return totalArea;
	}
	public void addShape(Object o) {
		objects.add(o);
	}
}

